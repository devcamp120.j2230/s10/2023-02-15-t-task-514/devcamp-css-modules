import avatar from "./assets/images/avatar.jpg";
import appStyle from "./App.module.css";

function App() {
  return (
    <div className={appStyle.devcampContainer}>
      <div>
        <img src={avatar} alt="avatar" className={appStyle.devcampAvatar}></img>
      </div>
      <div>
        <p className={appStyle.devcampQuote}>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className={appStyle.devcampUser}>
        <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;
